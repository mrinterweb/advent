#! /usr/bin/env ruby
require 'set'

Claim = Struct.new(:id, :left, :top, :width, :height)

CLAIM_PARSER = /^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)/

claims = Set.new

File.read('inputs.txt').each_line do |line|
  claim_args = line.strip.match(CLAIM_PARSER).to_a[1..-1].map(&:to_i)
  claims << Claim.new(*claim_args)
end

plane = []

claims.each do |claim|
  width_array = (claim.left...(claim.left + claim.width)).to_a
  claim.height.times do |i|
    row_index = claim.top + i
    plane[row_index] ||= []
    plane[row_index] += width_array
  end
end

total_square_inches = plane.sum do |row|
  # find uniq dups length
  row.select.with_index { |e, i| i != row.index(e) }.uniq.length
end

puts "Total overlapping square inches: #{total_square_inches}"
