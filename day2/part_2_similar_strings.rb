#! /usr/bin/env ruby

require 'set'
require 'text'

lines = File.read('input.txt').each_line.map(&:strip)

known_matches = Set.new

lines.each do |line1|
  next if known_matches.include?(line1)
  lines.select do |line2, arr2|
    next if known_matches.include?(line2)
    if Text::Levenshtein.distance(line1, line2, 2) == 1
      known_matches << line1
      known_matches << line2
    end
  end
end

puts known_matches
