#! /usr/bin/env ruby
two_char_count = 0
three_char_count = 0

File.read('input.txt').each_line do |line|
  two_satisfied = false
  three_satisfied = false

  line.split('').group_by { |c| c }.values.each do |char_group|
    if char_group.length == 2 && !two_satisfied
      two_char_count += 1
      two_satisfied = true
    elsif char_group.length == 3 && !three_satisfied
      three_char_count += 1
      three_satisfied = true
    end
  end
end

puts "checksum: #{two_char_count * three_char_count}"
