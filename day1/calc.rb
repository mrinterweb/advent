# --- Part 1
frequency = 0
inputs = []

frequency_history = [0]

File.open('inputs.txt').read.each_line do |line|
  input = line.to_i
  inputs << input
  frequency += input
  frequency_history << frequency
end

puts "Part 1: Final frequency: #{frequency}"

# --- Part 2
grouped_inputs = frequency_history.group_by { |f| f }
first_dup = grouped_inputs.select { |_k, v| v.size > 1 }.first

if first_dup
  puts "Part 2: First duplicate frequency: #{first_dup}"
else # need to keep processing inputs until a dup is found
  catch :dup_detected do
    input_loop_count = 1
    loop do
      puts "input loop: #{input_loop_count += 1}"
      inputs.each do |input|
        frequency += input
        grouped_inputs[frequency] ||= []
        grouped_inputs[frequency] << frequency

        if grouped_inputs[frequency].length > 1
          puts "Part 2: First duplicate frequency: #{frequency}"
          throw :dup_detected
        end
      end
    end
  end
end
